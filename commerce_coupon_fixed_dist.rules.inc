<?php

/**
 * @file
 * Coupon fixed distributed rules integration file.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_coupon_fixed_dist_rules_action_info() {
  $actions = array();
  $actions['commerce_coupon_fixed_dist_apply_to_product_line_item'] = array(
    'label' => t('Apply a fixed distributed coupon to a product line item'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
      'line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line item'),
      ),
      'coupon' => array(
        'type' => 'commerce_coupon',
        'label' => t('Coupon'),
      ),
      'component_name' => array(
        'type' => 'text',
        'label' => t('Price component type'),
        'description' => t('Price components track changes to prices made during the price calculation process, and they are carried over from the unit price to the total price of a line item. When an order total is calculated, it combines all the components of every line item on the order. When the unit price is altered by this action, the selected type of price component will be added to its data array and reflected in the order total display when it is formatted with components showing. Defaults to base price, which displays as the order Subtotal.'),
        'options list' => 'commerce_price_component_titles',
        'default value' => 'base_price',
      ),
      'round_mode' => array(
        'type' => 'integer',
        'label' => t('Price rounding mode'),
        'description' => t('Round the resulting price amount after performing this operation.'),
        'options list' => 'commerce_round_mode_options_list',
        'default value' => COMMERCE_ROUND_HALF_UP,
      ),
    ),
    'base' => 'commerce_coupon_fixed_dist_apply_to_product_line_item',
    'group' => t('Commerce Coupon'),
  );

  return $actions;
}

function commerce_coupon_fixed_dist_apply_to_product_line_item($order, $line_item, $coupon, $component_name, $round_mode) {
 
  // Calculate the total of the order products base price(s).
  $order_line_item_total = 0;
  if (isset($order->commerce_line_items[LANGUAGE_NONE]) && is_array($order->commerce_line_items[LANGUAGE_NONE])) {
    // Iterate around the line items.
    foreach ($order->commerce_line_items[LANGUAGE_NONE] as $value) {
      $order_line_item = commerce_line_item_load($value['line_item_id']);
      // If the line item is a product line item...
      if (isset($order_line_item) && in_array($order_line_item->type, commerce_product_line_item_types())) {
        $wrapper = entity_metadata_wrapper('commerce_line_item', $order_line_item);
        $unit_price = commerce_price_wrapper_value($wrapper, 'commerce_unit_price', TRUE);
        $order_line_item_total += $unit_price['data']['components'][0]['price']['amount'] * $order_line_item->quantity;
      }
    }
  }

  $coupon_wrapper = entity_metadata_wrapper('commerce_coupon', $coupon);
  $fields = $coupon_wrapper->getPropertyInfo();
  // Apply the coupon just if it's active, the type is of fixed distributed and it has the
  // field for fixed distributed amount set.
  if ($coupon->is_active == TRUE && $coupon->type == 'commerce_coupon_fixed_dist'
      && isset($fields['commerce_coupon_fixed_dist_amnt']) && isset($coupon_wrapper->commerce_coupon_fixed_dist_amnt) && is_array($coupon_wrapper->commerce_coupon_fixed_dist_amnt->value())) {
    $fixed_amnt = $coupon_wrapper->commerce_coupon_fixed_dist_amnt->value();

    // Get the price component to use in this price.
    $price_component_name = $coupon_wrapper->price_component_name->value();
    $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    $unit_price = commerce_price_wrapper_value($wrapper, 'commerce_unit_price', TRUE);

    $amount = commerce_round($round_mode, $unit_price['data']['components'][0]['price']['amount'] / $order_line_item_total * $fixed_amnt['amount']);

    // Calculate the updated amount and create a price array representing the
    // difference between it and the current amount.
    $current_amount = $unit_price['amount'];
    $updated_amount = commerce_round($round_mode, $current_amount - $amount);

    $difference = array(
      'amount' => $updated_amount - $current_amount,
      'currency_code' => $unit_price['currency_code'],
      'data' => array(),
    );

    // Set the amount of the unit price and add the difference as a component.
    $wrapper->commerce_unit_price->amount = $updated_amount;

    $wrapper->commerce_unit_price->data = commerce_price_component_add(
        $wrapper->commerce_unit_price->value(), $price_component_name, $difference, TRUE
    );
  }
}
